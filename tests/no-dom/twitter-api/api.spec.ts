import dotenv from "dotenv";
dotenv.config({ path: ".env.local" });

import { tweet } from "../../../src/twitter-api/api";

describe("tweet", () => {
  it("actually tweets (Be careful for a duplicate tweet)", async () => {
    if (!process.env.VUE_APP_OAUTH_CONSUMER_KEY)
      throw new Error("No env parameter: VUE_APP_OAUTH_CONSUMER_KEY");
    if (!process.env.VUE_APP_OAUTH_CONSUMER_SECRET)
      throw new Error("No env parameter: VUE_APP_OAUTH_CONSUMER_SECRET");
    if (!process.env.VUE_APP_OAUTH_TOKEN)
      throw new Error("No env parameter: VUE_APP_OAUTH_TOKEN");
    if (!process.env.VUE_APP_OAUTH_TOKEN_SECRET)
      throw new Error("No env parameter: VUE_APP_OAUTH_TOKEN_SECRET");

    try {
      await tweet(
        {
          consumerKey: process.env.VUE_APP_OAUTH_CONSUMER_KEY,
          consumerSecret: process.env.VUE_APP_OAUTH_CONSUMER_SECRET,
          token: process.env.VUE_APP_OAUTH_TOKEN,
          tokenSecret: process.env.VUE_APP_OAUTH_TOKEN_SECRET
        },
        "Test"
      );
    } catch (error) {
      console.error(error.response.data.errors);
      throw error;
    }
  });
});
