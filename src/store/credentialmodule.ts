import { Module, VuexModule, Mutation } from "vuex-module-decorators";
import { Cred } from "../twitter-api/cred";

@Module({ name: "credential" })
export default class CredentialModule extends VuexModule {
  currentCredential: Cred | null = null;

  @Mutation
  initializeCredential(): void {
    if (!process.env.VUE_APP_OAUTH_CONSUMER_KEY)
      throw new Error("No env parameter: VUE_APP_OAUTH_CONSUMER_KEY");
    if (!process.env.VUE_APP_OAUTH_CONSUMER_SECRET)
      throw new Error("No env parameter: VUE_APP_OAUTH_CONSUMER_SECRET");
    if (!process.env.VUE_APP_OAUTH_TOKEN)
      throw new Error("No env parameter: VUE_APP_OAUTH_TOKEN");
    if (!process.env.VUE_APP_OAUTH_TOKEN_SECRET)
      throw new Error("No env parameter: VUE_APP_OAUTH_TOKEN_SECRET");

    this.currentCredential = {
      consumerKey: process.env.VUE_APP_OAUTH_CONSUMER_KEY,
      consumerSecret: process.env.VUE_APP_OAUTH_CONSUMER_SECRET,
      token: process.env.VUE_APP_OAUTH_TOKEN,
      tokenSecret: process.env.VUE_APP_OAUTH_TOKEN_SECRET
    };
  }
}
