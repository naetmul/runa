import Vue from "vue";
import Vuex from "vuex";
import CredentialModule from "./credentialmodule";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    credential: CredentialModule
  }
});
