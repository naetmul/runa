import { User } from "./user";

export interface Tweet {
  created_at: string; // UTC time when this Tweet was created.
  id_str: string;
  full_text: string; // https://developer.twitter.com/en/docs/tweets/tweet-updates
  user: User;

  retweeted_status?: Tweet; // Retweets can be distinguished from typical Tweets by the existence of a `retweeted_status` attribute.
}
