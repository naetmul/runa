/**
 * Twitter Credentials
 */
export interface Cred {
  consumerKey: string;
  consumerSecret: string;
  token: string;
  tokenSecret: string;
}
