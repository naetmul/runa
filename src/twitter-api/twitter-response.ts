import * as dateFns from "date-fns";

export interface TwitterResponse<T, H> {
  result: T;
  header: H;
}

// https://developer.twitter.com/en/docs/basics/apps/guides/app-permissions
export interface AccessLevel {
  "x-access-level": "read" | "read-write" | "read-write-directmessages";
}

// https://developer.twitter.com/en/docs/basics/rate-limiting
export interface RateLimitingOriginal {
  "x-rate-limit-limit": string; // the rate limit ceiling for that given endpoint
  "x-rate-limit-remaining": string; // the number of requests left for the 15 minute window
  "x-rate-limit-reset": string; // the remaining window before the rate limit resets, in UTC epoch seconds
}

export interface RateLimiting {
  "x-rate-limit-limit": number;
  "x-rate-limit-remaining": number;
  "x-rate-limit-reset": Date;
}

export function hasRateLimiting(headers: object): boolean {
  const keys = Object.keys(headers);
  return (
    keys.includes("x-rate-limit-limit") &&
    keys.includes("x-rate-limit-remaining") &&
    keys.includes("x-rate-limit-reset")
  );
}

export function extractRateLimiting(
  headers: RateLimitingOriginal
): RateLimiting {
  return {
    "x-rate-limit-limit": Number(headers["x-rate-limit-limit"]),
    "x-rate-limit-remaining": Number(headers["x-rate-limit-remaining"]),
    "x-rate-limit-reset": dateFns.fromUnixTime(
      Number(headers["x-rate-limit-reset"])
    )
  };
}

export type OptionalRateLimiting = { "rate-limit"?: RateLimiting };

export interface ErrorResponse {
  status: number;
  message: string;
  errors: TwitterError[];
}

export interface TwitterError {
  code: number;
  message: string;
}
