export interface User {
  id_str: string;
  name: string;
  screen_name: string;
  profile_image_url_https: string;
}
