import axios, { AxiosResponse } from "axios";
import qs from "qs";
import * as util from "util";
import { Either, left, right } from "fp-ts/lib/Either";
import { buildHeaderString } from "./authorization";
import { Tweet } from "./tweet";
import {
  ErrorResponse,
  OptionalRateLimiting,
  hasRateLimiting,
  extractRateLimiting
} from "./twitter-response";
import { Cred } from "./cred";

/**
 * POST statuses/update
 *
 * https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/post-statuses-update
 */
export async function tweet(
  cred: Cred,
  text: string
): Promise<Either<ErrorResponse, Tweet>> {
  const httpMethod = "POST";
  const baseUrl = "https://api.twitter.com/1.1/statuses/update.json";
  const parameters = { status: text, tweet_mode: "extended" };
  const authorizationHeader = buildHeaderString(
    cred,
    httpMethod,
    baseUrl,
    parameters
  );

  try {
    const response: AxiosResponse<Tweet> = await axios({
      method: httpMethod,
      url: baseUrl,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: authorizationHeader
      },
      data: qs.stringify(parameters)
    });
    console.debug(util.inspect(response));

    const tweet = response.data;
    return right(tweet);
  } catch (error) {
    console.error(util.inspect(error));

    const result = {
      status: Number(error.response.status), // HTTP Status Code
      message: error.message,
      errors: error.response.data.errors // Twitter API error
    };

    return left(result);
  }
}

/**
 * GET statuses/home_timeline
 *
 * https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-home_timeline
 */
export async function getHomeTimeline(
  cred: Cred
): Promise<Either<ErrorResponse, Tweet[]> & OptionalRateLimiting> {
  const httpMethod = "GET";
  const baseUrl = "https://api.twitter.com/1.1/statuses/home_timeline.json";
  const parameters = { count: 5, tweet_mode: "extended" };
  const authorizationHeader = buildHeaderString(
    cred,
    httpMethod,
    baseUrl,
    parameters
  );

  try {
    const response: AxiosResponse<Tweet[]> = await axios({
      method: httpMethod,
      url: baseUrl,
      headers: {
        Authorization: authorizationHeader
      },
      params: parameters
    });
    console.log(util.inspect(response));

    const tweets = response.data;
    if (hasRateLimiting(response.headers)) {
      return {
        ...right(tweets),
        "rate-limit": extractRateLimiting(response.headers)
      };
    } else {
      return right(tweets);
    }
  } catch (error) {
    console.error(util.inspect(error));

    const result = {
      status: Number(error.response.status), // HTTP Status Code
      message: error.message,
      errors: error.response.data.errors // Twitter API error
    };

    if (hasRateLimiting(error.response.headers)) {
      return {
        ...left(result),
        "rate-limit": extractRateLimiting(error.response.headers)
      };
    } else {
      return left(result);
    }
  }
}
