# Runa

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
- Vue.js
```
npm run serve
```
- Electron
```
npm run electron:serve
```

### Compiles and minifies for production
- Vue.js
```
npm run build
```
- Electron
```
npm run electron:build
```

### Run your unit tests

```
npm run test:unit
```

Note the tests are run inside Node.js with browser environment simulated with JSDOM.

### Run your unit tests without JSDOM

```
npm run test:no-dom
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
